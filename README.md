# vdrop.link

## About

[vdrop.link](http://www.vdrop.link) let's users upload and download files using the [Veilid](https://veilid.com) network.

### How it works

#### Uploading a file

1. A user selects a file to upload.
1. Veilid's crypto library is used to generate a random shared secret and a nonce.
1. That secret and nonce is used to encrypt the file.
1. The encrypted file is then split into 1MB chunks (1MB per DHT key).
1. Each DHT chunk is then split into 32KB chunks (32KB per DHT subkey).
1. A new DHT record is created per DHT chunk, and all subkey chunks are set as that DHT record's value.
1. The file metadata, and list of DHT record locations for the chunks above are combined into a JSON encoded header, which is also encrypted with the shared secret and a new nonce.
1. A new DHT record is created, and encrypted file header data is loaded into it.
1. The user is presented with a file key in the format of `CryptoKind:DhtKeyOfHeader:SharedSecret`

#### Downloading a file

The above, but in reverse.

### Limitations

#### Can't run on HTTPS or use SSL certificates

Veilid doesn't have support yet for Outbound Relays, which is what would be needed in order to run a veilid-wasm node on an HTTPS origin. [See the veilid-wasm README for more details](https://gitlab.com/veilid/veilid/-/blob/main/veilid-wasm/README.md#running-wasm-on-https-sites-not-currently-implemented).

#### Each new tab or page refresh creates a new veilid node

This is a workaround for this issue [WASM can run multiple times](https://gitlab.com/veilid/veilid/-/issues/175). I have some experimental code to coordinate veilid instances across browser tabs using [BroadcastChannels](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API), but it still needs some work. Ideally we could use [SharedWorkers](https://developer.mozilla.org/en-US/docs/Web/API/SharedWorker), but that requires HTTPS, which requires Veilid to launch support for Outbound Relays.

#### Files over 5MB+ have a higher chance of failing to download.

This is due to an unknown bug in the Veilid DHT that is currently being investigated. The larger the file, the more DHT keys it's spread across, which means more opportunities for `KeyNotFound` errors when Veilid is trying to get the key's value from the network.

## Development guide

### Getting started

1. Run `npm install` to install dependencies.
1. Run `npm run dev` to start the dev server

### Included `veilid-wasm`

Out of the box, this repo includes a precompiled version of `veilid-wasm` in `src/veilid/veilid-wasm/included-release/`, as there is no npm package for it yet.

#### Compiling your own `veilid-wasm`

If you want to swap out the precompiled `release` or `debug` version of `veilid-wasm`:

1. Checkout this repo next to the `veilid` repo on your system. The existing symlinks in `src/veilid/veilid-wasm/` expect this.
1. Go into your `veilid` repo, into the `veilid-wasm` directory, and run `./wasm_build.sh` for debug version, or `./wasm_build.sh release` for release version without debug symbols.
1. In this repo, go into `src/veilid/veilid-wasm/index.ts` and change `included-release` to `debug` or `release` in the import statements. That will use a symlink that points to the WASM build artifacts from `./wasm_build.sh`.

### Deployment

#### Build for production

```bash
npm run build
```

#### AWS auth

```bash
aws sso login
```

#### Deploy to staging

```bash
aws s3 cp dist/ s3://staging.vdrop.link/ --recursive
```

#### Deploy to production

```bash
aws s3 cp dist/ s3://www.vdrop.link/ --recursive
```
