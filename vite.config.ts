import { defineConfig, searchForWorkspaceRoot } from 'vite';
import react from '@vitejs/plugin-react';
import wasm from 'vite-plugin-wasm';
import path from 'path';

// search up for workspace root
const workspaceRoot = searchForWorkspaceRoot(process.cwd());

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), wasm()],
  server: {
    fs: {
      allow: [
        workspaceRoot,
        path.resolve(
          workspaceRoot,
          '../veilid/target/wasm32-unknown-unknown/debug/pkg/'
        ),
        path.resolve(
          workspaceRoot,
          '../veilid/target/wasm32-unknown-unknown/release/pkg/'
        ),
      ],
    },
  },
});
