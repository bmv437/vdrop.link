import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';

import { initVeilid } from './veilid';
import { VeilidProvider } from './hooks/veilid-hooks.tsx';
import {
  ChakraProvider,
  Theme,
  defineStyleConfig,
  extendTheme,
} from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const components = {
  Container: defineStyleConfig({
    baseStyle: (props) => ({
      display: 'flex',
      flexDirection: 'column',
      background: mode('white', 'purple.800')(props),
      border: mode('1px', '0px')(props),
      borderColor: mode('gray.100', '')(props),
      gap: 6,
      borderRadius: 16,
      p: 4,
      width: '500px',
    }),
  }),
};

const config: Theme['config'] = {
  initialColorMode: 'system',
  useSystemColorMode: true,
};

const theme = extendTheme({
  config,
  components,
});

const queryClient = new QueryClient();

(async function main() {
  // Load the WASM module
  await initVeilid();

  ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
      <ChakraProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <VeilidProvider>
            <App />
          </VeilidProvider>
        </QueryClientProvider>
      </ChakraProvider>
    </React.StrictMode>
  );
})();
