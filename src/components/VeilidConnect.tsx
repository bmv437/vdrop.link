import { startVeilid } from '../veilid';
import { Button } from '@chakra-ui/react';
import { useVeilidStateSelector } from '../hooks/veilid-hooks';
import * as React from 'react';

export const VeilidConnect: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const isConnected = useVeilidStateSelector((state) => {
    return state.attachment?.state
      ? state.attachment.state !== 'Detached' &&
          state.attachment.state !== 'Detaching' &&
          state.attachment.state !== 'Attaching'
      : false;
  });

  React.useEffect(() => {
    if (isConnected) {
      setIsConnecting(false);
    }
  }, [isConnected]);

  const [isConnecting, setIsConnecting] = React.useState(false);

  const handleConnect = React.useCallback(async () => {
    try {
      setIsConnecting(true);
      await startVeilid();
    } catch (e) {
      setIsConnecting(false);
    }
  }, [setIsConnecting]);

  if (isConnected) {
    return children;
  }

  return (
    <Button
      isDisabled={isConnecting || isConnected}
      isLoading={isConnecting}
      onClick={handleConnect}
    >
      Connect
    </Button>
  );
};
