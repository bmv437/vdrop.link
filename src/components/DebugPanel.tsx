import { Box, Divider, Heading, useStyleConfig } from '@chakra-ui/react';
import React from 'react';
import { VeilidStatus } from './VeilidStatus';
import { DebugConsole } from './DebugConsole';
import { VeilidConfig } from './VeilidLogLevel';

export const DebugPanel: React.FC = () => {
  const containerStyles = useStyleConfig('Container');
  return (
    <Box __css={containerStyles}>
      <Heading as="h2" size={'lg'}>
        Debug panel
      </Heading>

      <Divider />
      <VeilidStatus />
      <Divider />
      <VeilidConfig />
      <Divider />
      <DebugConsole />
    </Box>
  );
};
