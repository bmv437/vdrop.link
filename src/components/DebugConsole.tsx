import { useForm } from 'react-hook-form';
import { veilidClient } from '../veilid';
import {
  Button,
  Heading,
  Input,
  Stack,
  Textarea,
  Text,
  Code,
} from '@chakra-ui/react';
import React from 'react';

export const DebugConsole = () => {
  const { register, handleSubmit, reset } = useForm<{ command: string }>();
  const [commandLog, setCommandLog] = React.useState('');
  const commandLogRef = React.useRef<HTMLTextAreaElement>();

  React.useLayoutEffect(() => {
    if (commandLogRef.current) {
      commandLogRef.current.scrollTop = commandLogRef.current?.scrollHeight;
    }
  }, [commandLog]);

  return (
    <Stack>
      <Heading as="h3" size="md">
        Debug command console
      </Heading>
      <Text>
        Run internal debug commands on this veilid node. Command output is also
        printed to the browser console. Use the <Code>help</Code> command for
        more info.
      </Text>
      <Textarea
        readOnly
        fontSize={'xs'}
        fontStyle={'mono'}
        rows={10}
        ref={commandLogRef as React.LegacyRef<HTMLTextAreaElement>}
        value={commandLog}
      />
      <form
        onSubmit={handleSubmit(async (data) => {
          try {
            const response = await veilidClient.debug(data.command);

            console.log(response);
            setCommandLog(
              [commandLog, `\n>> ${data.command}\n\n`, response].join('')
            );
          } catch (err) {
            console.log('Error', err);
            setCommandLog(
              [
                commandLog,
                `\n>> ${data.command}\n\n`,
                (err as Error).message,
              ].join('')
            );
          }
          reset();
        })}
      >
        <Stack direction={'row'}>
          <Input
            {...register('command')}
            placeholder="<command>"
            className="message-input"
          />
          <Button type="submit">Run</Button>
        </Stack>
      </form>
    </Stack>
  );
};
