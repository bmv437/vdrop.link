import { QuestionIcon } from '@chakra-ui/icons';
import { IconButton, IconButtonProps, Tooltip } from '@chakra-ui/react';
import React from 'react';

interface DebugButtonProps extends Partial<IconButtonProps> {}

export const DebugButton: React.FC<DebugButtonProps> = (props) => {
  return (
    <Tooltip label="Show/hide the debug panel" placement="left">
      <IconButton
        position={'fixed'}
        right={'16px'}
        bottom={'16px'}
        isRound
        variant={'ghost'}
        icon={<QuestionIcon color="gray.500" boxSize={6} />}
        aria-label="Show/hide the debug panel"
        {...props}
      />
    </Tooltip>
  );
};
