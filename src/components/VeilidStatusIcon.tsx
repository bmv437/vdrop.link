import { IconProps, Tooltip } from '@chakra-ui/react';
import { useVeilidStateSelector } from '../hooks/veilid-hooks';
import { AttachmentStateIcon } from './AttachmentStateIcon';
import { AttachmentState } from '../veilid';
import React from 'react';

const attachmentStateMessages: { [TKey in AttachmentState]: string } = {
  Detached: 'Disconnected',
  Attaching: 'Connecting',
  AttachedWeak: 'Connected',
  AttachedGood: 'Connected',
  AttachedStrong: 'Connected',
  FullyAttached: 'Connected',
  OverAttached: 'Connected',
  Detaching: 'Disconnecting',
};

interface VeilidStatusIconProps extends IconProps {}

export const VeilidStatusIcon: React.FC<VeilidStatusIconProps> = (props) => {
  const attachmentState =
    useVeilidStateSelector((state) => {
      return state.attachment?.state;
    }) ?? 'Detached';

  return (
    <Tooltip
      label={attachmentStateMessages[attachmentState]}
      aria-label="Veilid network connection status"
    >
      <span>
        <AttachmentStateIcon {...props} attachmentState={attachmentState} />
      </span>
    </Tooltip>
  );
};
