import * as React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useStoreFile } from '../hooks/file-storage-hooks';
import {
  Button,
  Code,
  Heading,
  Input,
  Link,
  Progress,
  Stack,
  Text,
  Textarea,
} from '@chakra-ui/react';
import _ from 'lodash';

interface FormValues {
  file?: File;
}

export const FileUpload: React.FC = () => {
  const {
    handleSubmit,
    control,
    formState: { isValid, isSubmitting },
  } = useForm<FormValues>();

  const {
    mutateAsync: storeFile,
    data: storeFileData,
    error,
    progress,
  } = useStoreFile();
  const onSubmit = async (formValues: FormValues) => {
    if (formValues.file) {
      try {
        await storeFile({ file: formValues.file });
      } catch (err) {
        console.log('Error occurred when trying to upload file', err);
      }
    }
  };

  const progressText = `${Math.floor(progress * 100)}%`;

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack>
        <Heading size="md">Upload to the DHT</Heading>
        <Text>
          Your file will be encrypted with a randomly generated secret before
          being uploaded.
        </Text>
        <Controller
          control={control}
          name={'file'}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, ...field } }) => {
            return (
              <Input
                {..._.omit(field, 'value')}
                onChange={(event) => {
                  onChange(event?.target?.files?.[0]);
                }}
                type="file"
              />
            );
          }}
        />
        <Button
          type="submit"
          isDisabled={!isValid}
          isLoading={isSubmitting}
          loadingText={`Uploading ${progressText}`}
        >
          Upload
        </Button>

        {isSubmitting && (
          <Progress colorScheme="purple" value={progress * 100} />
        )}

        {error && (
          <Text>
            {' '}
            An error occurred: <Code>{error.message}</Code>
          </Text>
        )}

        {storeFileData?.fileKey && (
          <Stack>
            <Text>Your file was uploaded successfully!</Text>
            <Text>
              This file key can be used to download and decrypt your file:
            </Text>
            <Textarea
              readOnly
              fontFamily={'mono'}
              rows={3}
              value={storeFileData.fileKey}
            />

            <Text>You can also share this file using this link:</Text>
            <Code>
              <Link
                fontFamily="mono"
                href={`${window.location.origin}/?fileKey=${encodeURIComponent(
                  storeFileData.fileKey
                )}`}
              >
                {`${window.location.origin}/?fileKey=${encodeURIComponent(
                  storeFileData.fileKey
                )}`}
              </Link>
            </Code>
          </Stack>
        )}
      </Stack>
    </form>
  );
};
