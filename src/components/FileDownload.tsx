// {key: 'VLD0:6vb5qDLto-F7mulL_nsb8_vjcl6l3zjh3GaMJ10liiw', accessKey: 'Ni1xbrSU8_IupNnnyOAxlcJuicLlyWxskqmCDVNZ4b4'}
import * as React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useLoadFile } from '../hooks/file-storage-hooks';
import {
  Button,
  Code,
  Heading,
  Progress,
  Stack,
  Text,
  Textarea,
} from '@chakra-ui/react';

interface FormValues {
  fileKey: string;
}

export const downloadFileFromUrl = (url: string, fileName: string): void => {
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = fileName;
  a.target = '_blank';
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  // window.URL.revokeObjectURL(a.href);
};

const initialFileKey = new URLSearchParams(window.location.search).get(
  'fileKey'
);

export const FileDownload = () => {
  const {
    handleSubmit,
    formState: { isSubmitting, isValid },
    control,
  } = useForm<FormValues>({
    values: {
      fileKey: initialFileKey || '',
    },
  });
  const [objectUrl, setObjectUrl] = React.useState<string>();
  // const { isLoading, progress, file, loadFile } = useLoadFile();

  const { mutateAsync: loadFile, data: file, progress, error } = useLoadFile();

  React.useEffect(() => {
    if (file) {
      const objectUrl = URL.createObjectURL(file);
      setObjectUrl(objectUrl);
    }
    return () => {
      if (objectUrl) URL.revokeObjectURL(objectUrl);
    };
  }, [file]);

  const onSubmit = async (data: FormValues) => {
    if (data.fileKey) {
      try {
        await loadFile({ fileKey: data.fileKey });
      } catch (err) {
        console.log('Error occurred when trying to download file', err);
      }
    }
  };

  const progressText = `${Math.floor(progress * 100)}%`;

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack>
        <Heading size="md">Download from the DHT</Heading>
        <Controller
          control={control}
          name={'fileKey'}
          defaultValue=""
          rules={{
            required: true,
          }}
          render={({ field }) => {
            return (
              <Textarea
                placeholder="Enter the file key"
                fontFamily={'mono'}
                resize={'vertical'}
                rows={3}
                {...field}
              />
            );
          }}
        />

        <Button
          type="submit"
          isDisabled={!isValid}
          isLoading={isSubmitting}
          loadingText={`Downloading ${progressText}`}
        >
          Download
        </Button>

        {isSubmitting && (
          <Progress colorScheme="purple" value={progress * 100} />
        )}
        {error && (
          <>
            <Text>An error occurred: </Text>
            <Code>
              <pre>{JSON.stringify(error, null, 2)}</pre>
            </Code>
          </>
        )}

        {!error && (
          <>
            {objectUrl && file && (
              <Button as="a" href={objectUrl} download={file.name}>
                Save file
              </Button>
            )}
            {file && file.type && file.type.startsWith('image') && (
              <img src={objectUrl} width="200px" />
            )}
          </>
        )}
      </Stack>
    </form>
  );
};
