import { IconProps, createIcon } from '@chakra-ui/react';
import { AttachmentState } from '../veilid';
import React from 'react';

// https://fonts.google.com/icons?selected=Material+Symbols+Outlined:signal_cellular_4_bar:FILL@0;wght@400;GRAD@0;opsz@24&icon.query=cellular

const createMaterialIcon = (options: Parameters<typeof createIcon>[0]) =>
  createIcon({
    viewBox: '0 -960 960 960',
    ...options,
    defaultProps: {
      boxSize: 6, // 24px
      ...options.defaultProps,
    },
  });

// const SignalBarOffIcon = createMaterialIcon({
//   displayName: 'SignalBarOffIcon',
//   path: (
//     <path
//       fill="currentColor"
//       d="M273-160h414L480-367 273-160ZM831-16l-64-64H80l344-344L96-751l56-57L888-72l-57 56Zm49-177-80-80v-414L593-480l-57-56 344-344v687ZM697-377ZM584-264Z"
//     />
//   ),
// });

const SignalBarNoneIcon = createMaterialIcon({
  displayName: 'SignalBarNoneIcon',
  path: (
    <path
      fill="currentColor"
      d="m676-100-56-56 84-84-84-84 56-56 84 84 84-84 56 56-83 84 83 84-56 56-84-83-84 83ZM80-80l800-799v427q-18-11-38-17.5T800-480v-206L273-160h257q8 23 20 43t27 37H80Zm193-80 527-526q-76 76-138 137.5t-121.5 121L417-304 273-160Z"
    />
  ),
});
const SignalBar0Icon = createMaterialIcon({
  displayName: 'SignalBar0Icon',
  path: (
    <path
      fill="currentColor"
      d="m80-80 800-800v800H80Zm193-80h527v-526L273-160Z"
    />
  ),
});

const SignalBar1Icon = createMaterialIcon({
  displayName: 'SignalBar1Icon',
  path: (
    <path
      fill="currentColor"
      d="m80-80 800-800v800H80Zm320-80h400v-526L400-286v126Z"
    />
  ),
});

const SignalBar2Icon = createMaterialIcon({
  displayName: 'SignalBar2Icon',
  path: (
    <path
      fill="currentColor"
      d="m80-80 800-800v800H80Zm440-80h280v-526L520-406v246Z"
    />
  ),
});

const SignalBar3Icon = createMaterialIcon({
  displayName: 'SignalBar3Icon',
  path: (
    <path
      fill="currentColor"
      d="m80-80 800-800v800H80Zm520-80h200v-526L600-486v326Z"
    />
  ),
});

const SignalBar4Icon = createMaterialIcon({
  displayName: 'SignalBar4Icon',
  path: <path fill="currentColor" d="m80-80 800-800v800H80Z" />,
});

const AttachmentIconMap: { [TKey in AttachmentState]?: React.ComponentType } = {
  Attaching: SignalBar0Icon,
  AttachedWeak: SignalBar1Icon,
  AttachedGood: SignalBar2Icon,
  AttachedStrong: SignalBar3Icon,
  FullyAttached: SignalBar4Icon,
  OverAttached: SignalBar4Icon,
};

interface Props extends IconProps {
  attachmentState: AttachmentState;
}
export const AttachmentStateIcon = ({
  attachmentState,
  ...iconProps
}: Props) => {
  const SignalIcon = AttachmentIconMap[attachmentState] ?? SignalBarNoneIcon;
  return <SignalIcon {...iconProps} />;
};
