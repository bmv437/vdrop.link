import { FormControl, FormLabel, Select, useToast } from '@chakra-ui/react';
import React from 'react';
import { VeilidLogLevel, veilidClient } from '../veilid';
import { veildCoreInitConfig } from '../veilid/veilid-config';

export const VeilidConfig: React.FC = () => {
  const toast = useToast({
    position: 'top-right',
  });
  const [logLevel, setLogLevel] = React.useState(
    veildCoreInitConfig.logging.api.level
  );

  const onLogLevelChange = React.useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const currentLogLevel = event.target.value as VeilidLogLevel;
      veildCoreInitConfig.logging.api.level = currentLogLevel;
      veilidClient.changeLogLevel('api', currentLogLevel);
      console.log('Updated API log level to:', currentLogLevel);
      toast({
        description: `Updated log level to: ${currentLogLevel}`,
        status: 'success',
        variant: 'subtle',
        duration: 3000,
        isClosable: true,
      });
      setLogLevel(currentLogLevel);
    },
    [setLogLevel]
  );

  return (
    <FormControl>
      <FormLabel>Veild API log level</FormLabel>

      <Select value={logLevel} onChange={onLogLevelChange}>
        <option value="Off">Off</option>
        <option value="Error">Error</option>
        <option value="Warn">Warn</option>
        <option value="Info">Info</option>
        <option value="Debug">Debug</option>
        <option value="Trace">Trace</option>
      </Select>
    </FormControl>
  );
};
