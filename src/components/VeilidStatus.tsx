import { Code, Heading, Stack, Text } from '@chakra-ui/react';
import { useVeilidStateSelector } from '../hooks/veilid-hooks';

export const VeilidStatus = () => {
  const nodeId = useVeilidStateSelector(
    (state) =>
      (
        state.config?.config.network.routing_table
          .node_id as unknown as string[]
      )?.[0]
  );

  const attachmentState =
    useVeilidStateSelector((state) => {
      return state.attachment?.state;
    }) ?? 'Detached';

  return (
    <Stack>
      <Heading as="h3" size="md">
        Veilid node info
      </Heading>
      <Text>Node id: {nodeId && <Code>{nodeId}</Code>}</Text>
      <Text>
        Attachment state: <Code>{attachmentState}</Code>
      </Text>
    </Stack>
  );
};
