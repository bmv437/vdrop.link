import {
  PublicKey,
  SecretKey,
  getRoutingContext,
  veilidCrypto,
} from '../veilid';
import { textDecoder, textEncoder } from '../utils/marshalling-utils';
import pRetry from 'p-retry';

const SUBKEY_MAX_SIZE_BYTES = 32 * 1024; // 32KB
const DHTKEY_MAX_SIZE_BYTES = 1024 * 1024; // 1MB

function times<T>(n: number, iter: (i: number) => T): T[] {
  const result = [];
  for (let i = 0; i < n; i++) {
    result.push(iter(i));
  }
  return result;
}

export interface DhtFileHeader {
  file: {
    name: string;
    size: number;
    type: string;
    lastModified: number;
  };
  keys: string[]; // probably supports like 512MB, but let's limit to 32MB
}

type OnProgress = (progress: number) => void;

const chunkBytes = (buffer: ArrayBuffer, chunkSize: number): Uint8Array[] => {
  const bytes = new Uint8Array(buffer);
  const numChunks = Math.ceil(bytes.byteLength / chunkSize);
  const chunks: Uint8Array[] = [];
  for (let chunkIndex = 0; chunkIndex < numChunks; chunkIndex++) {
    const byteOffset = chunkIndex * chunkSize;
    const byteLength = Math.min(bytes.byteLength - byteOffset, chunkSize);

    const chunkBytes = bytes.subarray(byteOffset, byteOffset + byteLength);

    chunks.push(chunkBytes);
  }
  return chunks;
};

const encryptBytes = (
  kind: string,
  bytes: Uint8Array,
  sharedSecret: string
): Uint8Array => {
  const nonce = veilidCrypto.randomNonce(kind);
  const nonceBytes = textEncoder.encode(nonce);

  const encryptedBytes = veilidCrypto.encryptAead(
    kind,
    bytes,
    nonce,
    sharedSecret
  );

  const result = new Uint8Array(nonce.length + encryptedBytes.length);
  result.set(nonceBytes, 0);
  result.set(encryptedBytes, nonce.length);
  return result;
};

const descryptBytes = (
  kind: string,
  bytes: Uint8Array,
  sharedSecret: string
): Uint8Array => {
  const nonce = textDecoder.decode(
    bytes.subarray(0, veilidCrypto.NONCE_LENGTH_ENCODED)
  );
  const encryptedBytes = bytes.subarray(veilidCrypto.NONCE_LENGTH_ENCODED);

  return veilidCrypto.decryptAead(kind, encryptedBytes, nonce, sharedSecret);
};

const storeBytesIntoKeys = async (
  kind: string,
  bytes: Uint8Array,
  onProgress?: OnProgress
): Promise<string[]> => {
  const dataChunks = chunkBytes(bytes, DHTKEY_MAX_SIZE_BYTES);
  console.log('Split file across', dataChunks.length, 'DHT keys');

  let progress = 0;

  const keys = [];
  for (const data of dataChunks) {
    const subKeys = chunkBytes(data, SUBKEY_MAX_SIZE_BYTES);

    const routingContext = getRoutingContext();
    const record = await routingContext.createDhtRecord(
      {
        kind: 'DFLT',
        o_cnt: subKeys.length,
      },
      kind
    );

    try {
      await Promise.all(
        subKeys.map(async (subKey, index) => {
          return pRetry(
            async () => {
              const result = await routingContext.setDhtValue(
                record.key,
                index,
                subKey
              );

              if (result) {
                throw new Error(
                  `Error setting subkey: ${index + 1} for key: ${record.key}`
                );
              }
              progress += (1 / dataChunks.length) * (1 / subKeys.length);
              onProgress?.(progress);
            },
            {
              retries: 3,
            }
          );
        })
      );
    } finally {
      await routingContext.closeDhtRecord(record.key);
    }
    keys.push(record.key);
  }

  return keys;
};

const loadBytesFromKeys = async (keys: string[], onProgress: OnProgress) => {
  let progress = 0;
  const routingContext = getRoutingContext();
  const chunks = [];
  console.log('Loading file that is split across', keys.length, 'DHT keys');
  for (const key of keys) {
    const record = await routingContext.openDhtRecord(key);

    try {
      chunks.push(
        await Promise.all(
          times(record.schema.o_cnt, async (index) => {
            return pRetry(
              async () => {
                const value = await routingContext.getDhtValue(
                  record.key,
                  index,
                  true
                );
                if (value?.data) {
                  progress += (1 / keys.length) * (1 / record.schema.o_cnt);
                  onProgress?.(progress);
                  return value.data;
                } else {
                  throw new Error(
                    `Error fetching subkey: ${index + 1} for key: ${key}`
                  );
                }
              },
              {
                retries: 3,
              }
            );
          })
        )
      );
    } finally {
      await routingContext.closeDhtRecord(record.key);
    }
  }

  const blocks = chunks.reduce((blocks, chunk) => {
    blocks.push(...chunk);
    return blocks;
  }, []);

  const totalBytes = blocks.reduce(
    (total, block) => total + block.byteLength,
    0
  );
  let byteOffset = 0;

  const bytes = new Uint8Array(totalBytes);
  for (let i = 0; i < blocks.length; i++) {
    bytes.set(blocks[i], byteOffset);
    byteOffset += blocks[i].byteLength;
  }
  return bytes;
};

interface StoreDhtFileOptions {
  file: File;
  onProgress?: (progress: number) => void;
}
export const storeDhtFile = async ({
  file,
  onProgress,
}: StoreDhtFileOptions): Promise<string> => {
  onProgress?.(0);

  const bestKind = veilidCrypto.bestCryptoKind();
  const sharedSecret = veilidCrypto.randomSharedSecret(bestKind);

  const fileBuffer = await file.arrayBuffer();

  const fileBytes = new Uint8Array(fileBuffer);

  const fileEncrypted = encryptBytes(bestKind, fileBytes, sharedSecret);

  const keys = await storeBytesIntoKeys(bestKind, fileEncrypted, onProgress);

  // header
  const header: DhtFileHeader = {
    file: {
      name: file.name,
      size: file.size,
      type: file.type,
      lastModified: file.lastModified,
    },
    keys,
  };
  const headerBytes = textEncoder.encode(JSON.stringify(header));

  const headerSubKey = encryptBytes(bestKind, headerBytes, sharedSecret);

  const routingContext = getRoutingContext();

  const record = await routingContext.createDhtRecord(
    {
      kind: 'DFLT',
      // o_cnt: subKeys.length,
      o_cnt: 1,
    },
    bestKind
  );
  await routingContext.setDhtValue(record.key, 0, headerSubKey);
  await routingContext.closeDhtRecord(record.key);

  return formatFileKey(record.key, sharedSecret);
};

function formatFileKey(key: string, secret: string) {
  return `${key}:${secret}`;
}

function parseFileKey(fileKey: string) {
  const [kind, key, secret] = fileKey.split(':');

  return {
    key: `${kind}:${key}`,
    secret,
  };
}

export type FileKeyData = {
  key: string;
  secret: string;
};

export interface LoadFileOptions {
  fileKey: string;
  onProgress: (progress: number) => void;
}

interface LoadFileHeaderResult {
  header: DhtFileHeader;
  secret: SecretKey;
  key: PublicKey;
}

export const loadDhtFileHeader = async (
  fileKey: string
): Promise<LoadFileHeaderResult> => {
  const { key, secret }: FileKeyData = parseFileKey(fileKey);
  const routingContext = getRoutingContext();
  const record = await routingContext.openDhtRecord(key);

  try {
    const bestKind = veilidCrypto.bestCryptoKind();
    const headerSubKeyEnc = await routingContext.getDhtValue(key, 0, false);

    if (!headerSubKeyEnc?.data) {
      throw new Error('DHT File Header data is empty');
    }

    const headerSubKey = descryptBytes(bestKind, headerSubKeyEnc?.data, secret);
    const header: DhtFileHeader = JSON.parse(textDecoder.decode(headerSubKey));
    return {
      header,
      key,
      secret,
    };
  } finally {
    routingContext.closeDhtRecord(record.key);
  }
};

export const loadDhtFile = async ({
  fileKey,
  onProgress,
}: LoadFileOptions): Promise<File> => {
  onProgress(0);

  const bestKind = veilidCrypto.bestCryptoKind();
  const { header, secret } = await loadDhtFileHeader(fileKey);

  const fileBytesEnc = await loadBytesFromKeys(header.keys, onProgress);
  const fileData = descryptBytes(bestKind, fileBytesEnc, secret);

  const file = new File([fileData], header.file.name, {
    lastModified: header.file.lastModified,
    type: header.file.type,
  });

  return file;
};
