import { FileUpload } from './components/FileUpload';
import { FileDownload } from './components/FileDownload';
import {
  Box,
  Divider,
  Flex,
  Heading,
  Link,
  useStyleConfig,
  Text,
} from '@chakra-ui/react';
import './index.css';
import { VeilidConnect } from './components/VeilidConnect';

import { VeilidStatusIcon } from './components/VeilidStatusIcon';
import { ExternalLinkIcon } from '@chakra-ui/icons';
import { DebugButton } from './components/DebugButton';
import React from 'react';
import { DebugPanel } from './components/DebugPanel';

function App() {
  const containerStyles = useStyleConfig('Container');

  const [isDebugPanelOpen, setDebugPanelOpen] = React.useState(false);
  return (
    <>
      <DebugButton
        onClick={() => {
          setDebugPanelOpen(!isDebugPanelOpen);
        }}
      />
      <Box w="100%" h="100%" display="flex" alignItems="center" gap="16px">
        <Box m="auto" display="flex" gap={'16px'}>
          <Box gap="16px" display="flex" flexDir="column">
          <Box __css={containerStyles} gap="8px">
            <Flex>
              <Heading flexGrow={1}>vdrop.link</Heading>
              <Box alignSelf={'center'}>
                <VeilidStatusIcon boxSize={8} />
              </Box>
            </Flex>
            <Box fontSize={'14px'}>
              Upload and download files using{' '}
              <Link fontWeight={'bold'} href="https://veilid.com/" isExternal>
                Veilid <ExternalLinkIcon />
              </Link>
              .
            </Box>
          </Box>

          <Box __css={containerStyles}>
            <VeilidConnect>
              <FileUpload />
              <Divider />
              <FileDownload />
            </VeilidConnect>
            </Box>

            <Box __css={containerStyles} gap="6px">
              <Text as="em" fontSize={'sm'}>
                Disclaimer: This site is for demonstration purposes only, and is
                not production software. Use at your own risk.
              </Text>
              <Link isExternal href="https://gitlab.com/bmv437/vdrop.link">
                View the source code on GitLab.
              </Link>
            </Box>
          </Box>

          <Box gap="16px" display="flex" flexDir="column">
          {isDebugPanelOpen && <DebugPanel />}
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default App;
