import loadVeilidWasm from './included-release/veilid_wasm';

export { loadVeilidWasm };
export * from './included-release/veilid_wasm';
