import * as React from 'react';
import {
  Provider,
  ReactReduxContextValue,
  createSelectorHook,
} from 'react-redux';
import { configureStore, createAction, createReducer } from '@reduxjs/toolkit';
import {
  VeilidStateAttachment,
  VeilidStateConfig,
  VeilidStateNetwork,
  VeilidUpdate,
} from '../veilid';
import {
  VeilidUpdateType,
  veilidEventEmitter,
} from '../veilid/veilid-event-emitter';
import { UseSelectorOptions } from 'react-redux/es/hooks/useSelector';

//********************************
// VeilidState redux reducer
//********************************
const veilidUpdateAction = createAction<VeilidUpdate>('@@veilid/update');

type VeilidReducerState = {
  attachment?: VeilidStateAttachment;
  network?: VeilidStateNetwork;
  config?: VeilidStateConfig;
};

type VeilidReducerActions = typeof veilidUpdateAction;

const veilidReducer = createReducer<VeilidReducerState>(
  {} as VeilidReducerState,
  (builder) => {
    builder.addCase(veilidUpdateAction, (state, action) => {
      switch (action.payload.kind) {
        case 'Attachment':
          state.attachment = action.payload;
          break;
        case 'Network':
          state.network = action.payload;
          break;
        case 'Config':
          state.config = action.payload;
          break;
      }
      return state;
    });
  }
);

const veilidStore = configureStore<VeilidReducerState, VeilidReducerActions>({
  reducer: veilidReducer,
});

type VeilidContextValue = ReactReduxContextValue<
  VeilidReducerState,
  VeilidReducerActions
>;
const VeilidReduxContext = React.createContext<VeilidContextValue>(
  null as unknown as VeilidContextValue
);

//********************************
// Hooks and Provider component
//********************************

// Types for createSelectorHook fn don't support generics, so we need to cast and wrap it with our own types.
const useSelector = createSelectorHook(
  VeilidReduxContext as unknown as React.Context<ReactReduxContextValue<any>>
);

export function useVeilidStateSelector<
  TState = VeilidReducerState,
  TSelected = unknown
>(
  selector: (state: TState) => TSelected,
  options?: UseSelectorOptions<TSelected>
): TSelected {
  return useSelector(selector, options);
}

interface VeilidProviderProps {
  children: React.ReactNode;
}
export const VeilidProvider = ({ children }: VeilidProviderProps) => {
  return (
    <Provider context={VeilidReduxContext} store={veilidStore}>
      {children}
    </Provider>
  );
};

veilidEventEmitter.on('Attachment', (data) => {
  veilidStore.dispatch(veilidUpdateAction(data));
});
veilidEventEmitter.on('Network', (data) => {
  veilidStore.dispatch(veilidUpdateAction(data));
});
veilidEventEmitter.on('Config', (data) => {
  veilidStore.dispatch(veilidUpdateAction(data));
});

export const useOnVeilidUpdate = <
  TKind extends Exclude<
    VeilidUpdate['kind'],
    'Attachment' | 'Network' | 'Config'
  >
>(
  kind: TKind,
  onUpdate: (data: VeilidUpdateType<TKind>) => void
) => {
  React.useEffect(() => {
    veilidEventEmitter.on(kind, onUpdate);
    return () => {
      veilidEventEmitter.removeListener(kind, onUpdate);
    };
  }, [onUpdate]);
};
