import * as React from 'react';
import { loadDhtFile, storeDhtFile } from '../utils/dht-file-store';
import { MutationOptions, useMutation } from '@tanstack/react-query';

interface UseStoreFileInput {
  file: File;
}
interface UseStoreFileOutput {
  fileKey: string;
}
export const useStoreFile = (
  options?: Omit<
    MutationOptions<UseStoreFileOutput, Error, UseStoreFileInput>,
    'mutationFn' | 'mutationKey'
  >
) => {
  const [progress, setProgress] = React.useState(0);
  const result = useMutation({
    mutationKey: ['dht-file-store', 'store-file'],
    mutationFn: async ({ file }: UseStoreFileInput) => {
      setProgress(0);

      const fileKey = await storeDhtFile({
        file,
        onProgress: setProgress,
      });
      return { fileKey };
    },
    ...options,
  });

  return {
    ...result,
    progress,
  };
};

interface UseLoadFileInput {
  fileKey: string;
}
export const useLoadFile = (
  options?: Omit<
    MutationOptions<File, Error, UseLoadFileInput>,
    'mutationFn' | 'mutationKey'
  >
) => {
  const [progress, setProgress] = React.useState(0);

  const result = useMutation({
    mutationKey: ['dht-file-store', 'load-file'],
    mutationFn: async ({ fileKey }: UseLoadFileInput) => {
      setProgress(0);

      const file = await loadDhtFile({
        fileKey,
        onProgress: setProgress,
      });

      return file;
    },
    ...options,
  });

  return {
    ...result,
    progress,
  };
};
